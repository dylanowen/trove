package gnu.trove;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TObjectIntMap;
import junit.framework.TestCase;


/**
 *
 */
public class TCollectionsTest extends TestCase {
	public void testUnmodifiableList() {
		final TIntArrayList one = new TIntArrayList( new int[]{ 1, 2, 3, 4 } );
		final TIntArrayList two = new TIntArrayList( new int[]{ 1, 2, 3, 4 } );
		TIntList uOne = TCollections.unmodifiableList( one );
		TIntList uTwo = TCollections.unmodifiableList( two );

		assertEquals( one, two );
		assertEquals( uOne, uTwo );

	}


	public void testUnmodifiableSet() {
		final TIntSet one = new TIntHashSet( new int[]{ 1, 2, 3, 4 } );
		final TIntSet two = new TIntHashSet( new int[]{ 1, 2, 3, 4 } );
		TIntSet uOne = TCollections.unmodifiableSet( one );
		TIntSet uTwo = TCollections.unmodifiableSet( two );

		assertEquals( one, two );
		assertEquals( uOne, uTwo );
	}


	public void testUnmodifiableMap() {
		final TIntObjectMap<Integer> one = new TIntObjectHashMap<Integer>();
		one.put( 0, Integer.valueOf( 0 ) );
		one.put( 1, Integer.valueOf( 1 ) );
		one.put( 2, Integer.valueOf( 2 ) );
		one.put( 3, Integer.valueOf( 3 ) );
		final TIntObjectMap<Integer> two = new TIntObjectHashMap<Integer>( one );
		TIntObjectMap<Integer> uOne = TCollections.unmodifiableMap( one );
		TIntObjectMap<Integer> uTwo = TCollections.unmodifiableMap( two );

		assertEquals( one, two );
		assertEquals( uOne, uTwo );
	}

	public void testEmptyList() {
		final TIntList one = TCollections.emptyIntList();

		assertEquals(0, one.size());
	}

	public void testEmptyListIsUnmodifiable() {
		final TIntList one = TCollections.emptyIntList();

		try {
			one.add(1);
		} catch(UnsupportedOperationException e) {
			return;
		}

		fail();
	}

	public void testEmptySet() {
		final TIntSet one = TCollections.emptyIntSet();

		assertEquals(0, one.size());
	}

	public void testEmptySetIsUnmodifiable() {
		final TIntSet one = TCollections.emptyIntSet();

		try {
			one.add(1);
		} catch(UnsupportedOperationException e) {
			return;
		}

		fail();
	}

	public void testEmptyMap() {
		final TIntIntMap one = TCollections.emptyIntIntMap();

		assertEquals(0, one.size());
	}

	public void testEmptyMapIsUnmodifiable() {
		final TIntIntMap one = TCollections.emptyIntIntMap();

		try {
			one.put(1, 1);
		} catch(UnsupportedOperationException e) {
			return;
		}

		fail();
	}

	public void testEmptyMapTypeObject() {
		final TIntObjectMap one = TCollections.emptyIntObjectMap();

		assertEquals(0, one.size());
	}

	public void testEmptyMapTypeObjectIsUnmodifiable() {
		final TIntObjectMap<Integer> one = TCollections.emptyIntObjectMap();

		try {
			one.put(1, 1);
		} catch(UnsupportedOperationException e) {
			return;
		}

		fail();
	}

	public void testEmptyMapObjectType() {
		final TObjectIntMap one = TCollections.emptyObjectIntMap();

		assertEquals(0, one.size());
	}

	public void testEmptyMapObjectTypeIsUnmodifiable() {
		final TObjectIntMap<Integer> one = TCollections.emptyObjectIntMap();

		try {
			one.put(1, 1);
		} catch(UnsupportedOperationException e) {
			return;
		}

		fail();
	}
}
